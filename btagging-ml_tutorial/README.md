# b-tagging Machine Learning Tutorial


## Quick Start
1. [https://hub.cern.ch](https://hub.cern.ch) (you need to accept the certificate exception) and use the `atlas-ml-cpu` profile
2. Open a terminal on the Hub and type `bash` followed by `kinit <CERNLOGIN>` - will enable eos access 
3. Clone this repository `git clone https://gitlab.cern.ch/mguth/btagging-ml_tutorial.git` (All files not stored in your eos will be deleted when restarting the server!)
4. Start exploring the notebooks by opening them on the tab on the left hand side.

More detailed instructions below.

## Getting started

<!--Please open a new terminal on your local machine and log into lxplus-->
<!--```-->
<!--ssh -Y <CERNLOGIN>@lxplus.cern.ch-->
<!--```-->
<!--navigate to your `/eos` homefolder (or a subfolder)-->
<!--```-->
<!--cd /eos/user/<firstLetterCERNLOGIN>/<CERNLOGIN>/-->
<!--```-->
<!--and copy the following folder-->
You can access the training files on eos:
`/eos/user/m/mguth/public/btagging-ml_tutorial_files`


Alternatively you can also directly access the files on the hub under the following path `/eos/user/m/mguth/public/btagging-ml_tutorial_files`.

Afterwards in Jupyterlab (either on the Hub or from lxplus as described above) open a Terminal.
You can navigate to your eos home via
```
cd /eos/user/<firstLettreCERNLOGIN>/<CERNLOGIN>/
```
Then check out the git repository 
```
git clone https://gitlab.cern.ch/aml/tutorials/aml-workshop19-tutorials.git
```
Inside the repository you will find different Jupyter notebooks.
N.B. all files NOT saved on your eos will be erased when restarting the jupyter server!!!!

The notebook `Sample-preparation.ipynb` contains the pre-processing for DIPS and DL1 and the `train*` notebooks are for DIPS and DL1 training.

The notebooks can be started by navigating to the folder from the sidebar where they are in and then right click on it and chose `Open`.


## Some useful options

* [https://hub.cern.ch/hub/home](https://hub.cern.ch/hub/home) - create and manage multiple named servers
* shutting down server: `jupyter notebook stop 8888`
* if you can't get eos working, try using `curl`, [to download the files directly](https://dguest-public.web.cern.ch/dguest-public/ftag/tutorial/)i.e. `curl https://dguest-public.web.cern.ch/dguest-public/ftag/tutorial/MC16d_Zprime-test-validation_sample-NN.h5 > MC16d_Zprime-test-validation_sample-NN.h5`

